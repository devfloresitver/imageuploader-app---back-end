import express from 'express'

const router = express.Router();
//for file deletion
const fs = require('fs');
//storage path for uploaded images
const path = '../ImagePlatform/public/uploads/images/';

const mariadb = require('mariadb');
const pool = mariadb.createPool({
     host: 'localhost', 
     user:'root', 
     password: '',
     database: 'images',
     connectionLimit: 5
});


//for multipart
const multer = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/images');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
 
var upload = multer({ storage: storage })


//upload image
router.post('/upload', upload.single('imgUploader'), async (req, ores) => {
	//console.log(req);
    if(req.file) {
    	//inserting record
    	let conn;

    	conn = await pool.getConnection();

    	const rows = await conn.query(`INSERT INTO images VALUES("","${req.file.originalname}", "${'../uploads/images/'+req.file.originalname}")`)
    	.then(async (res) =>{

		try{
		conn = await pool.getConnection();
		const id = await conn.query(`SELECT * FROM images ORDER BY id DESC LIMIT 1`);
		req.file.id = id[0].id;
		req.file.imagePath = req.file.path+req.file.originalname;
		res.json = req.file;
		console.log(res.json);
		ores.send(res.json);
			}catch (err) {
			throw err;
		  } finally {
			 return conn.end();
		  }
			
    	})
    	.catch(err=>{
    		console.log(err);
    	})
    	.finally( () =>{
    		console.log('termine promise de cargar');
    		return conn.end();
    		});
    }
    else {
    	console.log('error uploading file');
    }
});

router.get('/searchImage', async(req, res)=>{
	//last record id
	let conn;
	try{
		conn = await pool.getConnection();
		const id = await conn.query(`SELECT * FROM images ORDER BY id DESC LIMIT 1`);
		res.json(id);
	}catch (err) {
	throw err;
  } finally {
	if (conn) return conn.end();
  }
});
   

//list images
router.get('/images', async(req, res)=>{
	let conn;
	try{
		console.log('ok');
		conn = await pool.getConnection();
		console.log('pool entro');
		const rows = await conn.query("SELECT * from images");
		console.log('encontre '+rows.length); //[ {val: 1}, meta: ... ]
	    res.json(rows);
	}catch (err) {
	throw err;
  } finally {
	if (conn) return conn.end();
  }
});

//delete image
router.post('/deleteImage/', async(req, res)=>{
	let conn;
	const id = req.body.id;
	const name = req.body.name;
	const imagePath = req.body.imagePath;
	console.log(req.body);
	try{
		conn = await pool.getConnection();
		const deletion = await conn.query(`DELETE FROM images WHERE ID=${id}`);
		//deleting file
		fs.unlink(path+name, (err)=>{
			if(err){
				console.log(path+name);
				console.error(err);
			}else{
				console.log('successfully deleted');
			}
		});
		console.table(deletion); 
	    res.send(res.json({
	    	'id' : id,
	    	'name' : name,
	    	'imagePath' : imagePath
	    }));
	}catch (err) {
	throw err;
  } finally {
	if (conn) return conn.end();
  }
});







/*
import Nota from '../models/nota';

router.post('/nueva-nota', async(req, res)=>{
	const body = req.body;
	try{
		const notaDB = await Nota.create(body);
		res.status(200).json(notaDB);
	}catch(error){
		return res.status(500).json({
			mensaje: 'Ocurrió un error',
			error
		})
	}
});

router.get('/nota/:id', async(req, res)=>{
	const _id = req.params.id;
	try{
		const notaDB = await Nota.findOne({_id});
		res.json(notaDB);
		return res.json;
	}catch(error){
		return res.status(400).json({
			mensaje: 'Ocurrió un error',
			error
		})
	}
});

router.get('/nota', async(req, res)=>{
	try{
		const notaDB = await Nota.find();
		res.json(notaDB);
	}catch(error){
		return res.status(400).json({
			mensaje: 'Ocurrió un error',
			error
		})
	}
});

router.delete('/nota/:id', async(req, res)=>{
	const _id = req.params.id;
	try{
		const notaDB = await Nota.findByIdAndDelete({_id});
		if(!notaDB){
			return res.status(400).json({
				mensaje: 'No se encontró el id indicado',
				error
			})
		}
	res.json(notaDB);
	}catch(error){
		return res.status(400).json({
			mensaje: 'Ocurrió un error',
			error
		})
	}
});

router.put('/nota/:id', async(req, res)=>{
	const _id = req.params.id;
	const body = req.body;
	try{
		const notaDB = await Nota.findByIdAndUpdate(
			_id,
			body,
			{new: true});
		res.json(notaDB);
	}catch(error){
		return res.status(400).json({
			mensaje: 'Ocurrió un error',
			error
		})
	}
});
*/
module.exports = router;